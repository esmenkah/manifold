
SHELL:=/bin/bash

CXX=mpic++
CC=g++
OBJDIR=obj
BINDIR=bin
SRCDIR=src
OUTDIR=output

CLFAGS0= -O2 -std=c++11 -Wall -Werror -Wextra  -D__BUFDEBUG 
CLFAGS1= -O2 -std=c++11 -Wall -Werror -Wextra   


LIB= -lmkl_blacs_openmpi_lp64 -lmkl_lapack95_lp64

LIB_PATH=/opt/intel/2017/compilers_and_libraries_2017/linux/mkl/lib/intel64 
INCLUDE=/opt/intel/2017/compilers_and_libraries_2017/linux/ipp/include:/opt/intel/2017/compilers_and_libraries_2017/linux/mkl/include

par: setup $(BINDIR)/manifold.arma.par.x
serial: setup $(BINDIR)/manifold.arma.serial.x

#need to load up openmpi/1.10.2/intel/2017
$(BINDIR)/manifold.arma.par.x: $(SRCDIR)/manifold_n3.par.cpp 
	. /usr/share/Modules/init/bash; \
	module purge; \
	module load openmpi/1.10.2/intel/2017; \
	$(CXX) $(CLFAGS0) $< -o $@ -I/home/emenkah/thibaud/armadillo-8.100.1/include -L/home/emenkah/thibaud/armadillo-8.100.1  -larmadillo 

#module load gcc/4.9.0 
#module load intel/2017 
$(BINDIR)/manifold.arma.serial.x: $(SRCDIR)/manifold_n3.serial.cpp
	. /usr/share/Modules/init/bash; \
	module purge; \
	module load gcc/4.9.0; \
	module load intel/2017; \
	$(CC) $(CLFAGS1) $< -o $@ -I/home/emenkah/thibaud/armadillo-8.100.1/include -L/home/emenkah/thibaud/armadillo-8.100.1  -larmadillo 


clean:
	rm -rf *.x $(OBJDIR) $(BINDIR)

test:
	#rm -f results.par.dat 
	#mpirun -np 2 manifold.par.x 1 1 8 0 2 0 

setup: 
	mkdir -p $(OBJDIR) $(BINDIR) $(OUTDIR) 
