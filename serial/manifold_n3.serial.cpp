#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iomanip>
#include <vector>
#include <armadillo>
//#include <mpi.h>

// version for Armadillo not up to date
// Needs C++11 and Armadillo: g++ manifold_n=5.cpp -o manifold_n=5 -Wall -Wextra -std=c++0x -O2 -larmadillo
// Compilation without installation of Armadillo: g++ manifold_n=5.cpp -o manifold_n=5 -Wall -Wextra -std=c++0x -O2 -I /scratch/tmaimbou/Armadillo/Library_for_compiler/usr/include/ -DARMA_DONT_USE_WRAPPER -lblas -llapack
// Computes the maximum over A of \FF_1 and then the minimum of it for arbitrary number of steps n (U[n], sigma[n]) by descending the logarithm of gradient.
// The updates of parameters are done with a step which is a bit optimized and not fixed
// Mixes arrays from C++ and vectors from Armadillo

const double PI=4.*atan(1.);
const int n=3;				// number of steps
const arma::uword m=2*n*10;			// number max of local maxima, otherwise work with vectors

using namespace std;
// using namespace arma;

// Computes q(A,y)=res[0] and dq(A,y)/dA=res[1] with arrays of values of U_i and sigma_i
// res is created before and stores these results in the end (passed by reference)
void fq(double (&res) [2], double& A,  double y, double (&U) [n], double (&sigma) [n]) {
    double nump=A+y;
    double numm=A-y;
    double den=2*sqrt(A);
    double sum=sigma[0];
    res[0]=1+exp(U[0])*erf(nump/den);
    res[1]=exp(U[0]-nump*nump/(4*A))*numm;
    for (int i=0;i<=n-2;i++) {
      res[0]+=(exp(U[i+1])-exp(U[i]))*erf((nump-sum)/den);
      res[1]+=(exp(U[i+1])-exp(U[i]))*exp(-(nump-sum)*(nump-sum)/(4*A))*(numm+sum);
      sum+=sigma[i+1];
    }
    res[0]+=(1-exp(U[n-1]))*erf((nump-sum)/den);
    res[0]*=0.5;
    res[1]+=(1-exp(U[n-1]))*exp(-(nump-sum)*(nump-sum)/(4*A))*(numm+sum);
    res[1]/=4*sqrt(PI)*pow(A,1.5);
}

// Computes dq(A,y)/dU[j]=res[0] and d^2q(A,y)/dU[j]dA=res[1] giving the value of j and arrays of values of U_i and sigma_i
// res is created before and stores these results in the end (passed by reference)
void fdUq(double (&res) [2], double& A,  double y, int& j, double (&U) [n], double (&sigma) [n]) {
    double nump=A+y;
    double numm=A-y;
    double den=2*sqrt(A);
    double sum=0;
    for (int i=0;i<=j-1;i++) sum+=sigma[i];
    res[0]=exp(U[j])*(erf((nump-sum)/den)-erf((nump-sum-sigma[j])/den))/2;
    res[1]=exp(U[j])*(exp(-(nump-sum)*(nump-sum)/(4*A))*(numm+sum)-exp(-(nump-sum-sigma[j])*(nump-sum-sigma[j])/(4*A))*(numm+sum+sigma[j]))/(4*sqrt(PI)*pow(A,1.5));
}

// Computes dq(A,y)/dsigma[j]=res[0] and d^2q(A,y)/dsigma[j]dA=res[1] giving the value of j and arrays of values of U_i and sigma_i
// res is created before and stores these results in the end (passed by reference)
void fdsq(double (&res) [2], double& A,  double y, int& j, double (&U) [n], double (&sigma) [n]) {
    double num=A+y;
    double sum=sigma[0];
    res[0]=0;
    res[1]=0;
    for (int i=1;i<=j;i++) sum+=sigma[i];
    for (int i=j;i<=n-2;i++) {
        double temp=(exp(U[i+1])-exp(U[i]))*exp(-(num-sum)*(num-sum)/(4*A));
        res[0]+=temp;
        res[1]+=(1+A/2-(y-sum)*(y-sum)/(2*A))*temp;
        sum+=sigma[i+1];
    }  
    double temp=(1-exp(U[n-1]))*exp(-(num-sum)*(num-sum)/(4*A));
    res[0]+=temp;
    res[1]+=(1+A/2-(y-sum)*(y-sum)/(2*A))*temp;
    res[0]/=-2*sqrt(A*PI);
    res[1]/=4*sqrt(PI)*pow(A,1.5);
}

// Computes a particular value of \FF_1, dy specifies the discretization step of the integral
double F1(double& A, double (&U) [n], double (&sigma) [n], double& dy) {
    double res=0;
    double q0[2];
    double q1 [2];
    double q2 [2];  
    double sum=sigma[0];
    for (int i=1;i<n;i++) 
        sum+=sigma[i];
    fq(q0,A,-10-sum-A,U,sigma); 					// q0 initialization
    for (double y=-10-sum-A;y<=10+sum+A;y+=dy) {
        fq(q1,A,y+dy/2,U,sigma);
        fq(q2,A,y+dy,U,sigma);
        if (q0[0]>1e-20 && q1[0]>1e-20 && q2[0]>1e-20) { 
          res+=(dy/6)*(exp(y)*log(q0[0])*q0[1]+4*exp(y+dy/2)*log(q1[0])*q1[1]+exp(y+dy)*log(q2[0])*q2[1]);	// Simpson's interpolation, error ~ dy^5 times fourth derivative / 2880 (wikipedia)
        }  
        for (int i=0;i<2;i++) q0[i]=q2[i];										// not to compute two times the same thing
    }
    return -A*res;
}
// Computes a particular value of d\FF_1/dU[j]=res[0] and d\FF_1/d sigma[j]=res[1]
// dy specifies the discretization step of the integral
// res is created before and stores these results in the end (passed by reference)
void dF1(double (&res) [2], double& A, int j, double (&U) [n], double (&sigma) [n], double& dy) {
    double q0[2];
    double q1 [2];
    double q2 [2];
    double dUq0 [2];
    double dUq1 [2];
    double dUq2 [2];
    double dsq0 [2];
    double dsq1 [2];
    double dsq2 [2];
    double sum=sigma[0];
    res[0]=0;
    res[1]=0;
    for (int i=1;i<n;i++) sum+=sigma[i];
    fq(q0,A,-10-sum-A,U,sigma);
    fdUq(dUq0,A,-10-sum-A,j,U,sigma);					// Initializations
    fdsq(dsq0,A,-10-sum-A,j,U,sigma);
    for (double y=-10-sum-A;y<=10+sum+A;y+=dy) {
        fq(q1,A,y+dy/2,U,sigma);
        fq(q2,A,y+dy,U,sigma);
        fdUq(dUq1,A,y+dy/2,j,U,sigma);
        fdUq(dUq2,A,y+dy,j,U,sigma);
        fdsq(dsq1,A,y+dy/2,j,U,sigma);
        fdsq(dsq2,A,y+dy,j,U,sigma);
        if (q0[0]>0 && q1[0]>0 && q2[0]>0) {                                      // check if another condition is more suitable, e.g. on q[1]
            res[0]+=(dy/6)*(exp(y)*(dUq0[0]*q0[1]/q0[0]+log(q0[0])*dUq0[1])+4*exp(y+dy/2)*(dUq1[0]*q1[1]/q1[0]+log(q1[0])*dUq1[1])+exp(y+dy)*(dUq2[0]*q2[1]/q2[0]+log(q2[0])*dUq2[1]));		// Simpson's interpolation
            res[1]+=(dy/6)*(exp(y)*(dsq0[0]*q0[1]/q0[0]+log(q0[0])*dsq0[1])+4*exp(y+dy/2)*(dsq1[0]*q1[1]/q1[0]+log(q1[0])*dsq1[1])+exp(y+dy)*(dsq2[0]*q2[1]/q2[0]+log(q2[0])*dsq2[1]));
        }
        for (int i=0;i<2;i++) {
            q0[i]=q2[i];
            dUq0[i]=dUq2[i];
            dsq0[i]=dsq2[i];      
        }  
    }
    res[0]*=-A;
    res[1]*=-A;
}

// Gives by reference the local maxima of \FF_1 (in FFval) and their respective positions A (in Arr) which maximizes \FF_1 (by reference) for given parameters U_i, sigma_i
// Returns the total number of them : int countermax
// FFval and Arr are full of zeroes beyond countermax
arma::uword cages(arma::vec& FFval, arma::vec& Arr, double (&U) [n], double (&sigma) [n], double& dy, double& incrA) {

    FFval.zeros();
    Arr.zeros();
    double A;
    double Aold=0;
    arma::uword countermax=0;
    double FFold1=0;
    double FFold2=0;

    for (double lA=-6;lA<2;lA+=incrA) {					// lower than -6 ? -8 ?

        A=pow(10,lA);
        double FFA=F1(A,U,sigma,dy);
        if ((FFold2 < FFold1) && (FFA < FFold1)) {
            FFval(countermax)=FFold1;							// suppose less than m maxima
            Arr(countermax)=Aold;
            countermax++;
        }

        FFold2=FFold1;
        FFold1=FFA;
        Aold=A;
    }

    return countermax;  
}  

// version for Armadillo library not up to date, replaces arma::index_max
// finds maximum of the sub-array of size counter, returns its index
arma::uword maximum(arma::vec& FFval) {
    double max=FFval(0);
    arma::uword res=0;
    for (arma::uword i=1;i<FFval.n_elem;i++) {
        if (FFval(i)>max) {
            res=i;
            max=FFval(i);
        }
    }
    return res;  
}


double seconds(){

    struct timeval tmp;
    double sec;
    gettimeofday( &tmp, (struct timezone *)0 );
    sec = tmp.tv_sec + ((double)tmp.tv_usec)/1000000.0;
    return sec;
}


int main(int argc, char ** argv) {

    if(argc != 7) {
        cout << "\nWrong number of arguments. Usage: mpirun -np <processors> manifold.x 1 1 8 0 2 0\n " << endl;
        return 1;
    }
     
    //Timings 
    double t_start, t_init, t_end;

    // Extraction of data through a file 
    int index=atof(argv[6]);			// line in data to initialize the potential {0..data.size()-1}
    vector<vector<double> > data;
    //ifstream file("higher_values2_n=5.dat");// file name or path
    ifstream file("short_run.dat");// file name or path
    string line;

    while (getline(file, line)) {
        data.push_back(vector<double>());
        istringstream ss(line);
        double value;
        while (ss >> value) data.back().push_back(value);
    }
    file.close();

    double U [n];						// potential parameters
    double sigma[n];
    for (int i=0;i<n;i++) {		// initialization of the potential    
        U[i]=data[index][2*i]; 
        sigma[i]=data[index][2*i+1];
    }
  
    //   // Manual specification of data
    //   double U [n];						// potential parameters
    //   double sigma[n];
    //   U[0]=2.89576628257266;
    //   U[1]=0.162749283650321;
    //   U[2]=-0.0969009363977359;
    //   U[3]=-0.0738035420068005;
    //   sigma[0]=0.0225555498934512;
    //   sigma[1]=0.275451364509803;
    //   sigma[2]=0.873959381813113;
    //   sigma[3]=1.00394798062494;
  
    //n=atof(argv[1]);
    int eincrA=atof(argv[1]);// for incrA (more readable in log files than doubles) - (by default 2)
    int edy=atof(argv[2]);				// for dy  (by default 2 or 3)
    int eeps=atof(argv[3]);				// for eps (by default 8)
    int least_search=atof(argv[4]);	// controls the least exponent of the linear search (by default 10)
    int max_search=atof(argv[5]);				// controls the least exponent of the linear search (by default 3 with old version ; here arbitrary e.g. 8)
    double eps=pow(10,-eeps);				// controls the threshold distance between the maxima
    double incrA=pow(10,-eincrA); // increments of the cage A when exploring the plot of \FF_1 (usually before 0.01 but we might want finer like 0.001, which increases by a factor ~10 the total number of computations)
    double dy=pow(10,-edy);				// discretization step of the \FF_1 integral
    double maj [2*n];					// stores the gradient of \FF_1 evaluated at the maximum Amax and U, sigma. Structure: [...,U[i],sigma[i], ...].
							// also used for two (or more) maxima with the same height to get the correct direction of the descent 
							
    arma::uword max1, max2;				// only for the procedure with exactly two maxima with the same height
    double Amax1, Amax2, FFmax1;				// these parameters will also be updated at each loop
  
    double best=0;					// these parameters will also be updated at each loop
    double phi;
    arma::uword counter;					// counts number of local maxima, like countermax
    arma::vec FFval(m);					// these two contain extrema of FF_1 (value or A)
    arma::vec Arr(m);
    arma::vec Arrman;
    
    //if (rank ==0){ 
    ofstream manifold ("results.serial.dat", ios_base::app);
    //ofstream manifold ("manifold_n="+to_string(n)+"_lA="+to_string(eincrA)+"_dy="+to_string(edy)+"_eps="+to_string(eeps)+"_ls="+to_string(least_search)+"_ms="+to_string(max_search)+"_index="+to_string(index)+".dat",ios_base::app);
    manifold.close(); 
    //}

    // Initializations  

    t_start = seconds();
    counter = cages(FFval,Arr,U,sigma,dy,incrA);
    t_init = ( seconds() - t_start )/60.0;
    
    max1    = maximum(FFval);			// everything related to max1 is about the absolute maximum of FF_1
    FFmax1  = FFval(max1);
    Amax1   = Arr(max1);
    phi     = 1./FFmax1;
 
    int cycle = 0; 
    // The program ends if no highest phi than the previous one is found during the procedure
    while (best!=-1) {
   
        cycle++ ;
        cout << "Begining While loop, Cycle: " << cycle << endl;

        manifold.open("results.serial.dat", ios_base::app); 
        for (int j=0;j<n;j++) manifold << U[j]<<' '<<sigma[j]<<' ';
        manifold.close(); 
        manifold.flush();
    
        arma::uword countman=1;				// counts the number of maxima with the same height up to eps
    
        if (counter>1) {
            Arrman=Arr(find(FFval>FFmax1-eps));					//  same as Arr but with only values of A for which maxima are equal
            countman=Arrman.n_elem;
        }
            manifold.open("results.serial.dat", ios_base::app); 
            manifold<<setprecision(15)<<phi<<' '<<counter<<' '<<countman<<' '<<Amax1<<' '<<FFmax1<<' ';
            manifold.close(); 
            manifold.flush();
        // Normal gradient descent
        if (countman==1) {
            double tempgrad [2];
      
            // Computation of the gradient in Amax1
            for (int j=0;j<n;j++) {
                dF1(tempgrad,Amax1,j,U,sigma,dy);
                maj[2*j]=U[j]*tempgrad[0];
                maj[2*j+1]=sigma[j]*tempgrad[1];   
                //norm+=tempgrad[0]*tempgrad[0]+tempgrad[1]*tempgrad[1];
                manifold.open("results.serial.dat", ios_base::app); 
                manifold<<setprecision(15)<<tempgrad[0]<<' '<<tempgrad[1]<<' ';   
                manifold.close(); 
                manifold.flush();

            }
        }
    
        // Modified gradient descent for 2 maxima, same as descent.cpp
        else if (countman == 2) {
    
            // the following is copied from descent.cpp, in theory it is useless and could be replaced by the more complex gradient descent within the manifold
            // to be sure of obtaining the same result for two equal maxima we leave it this way (temporarily)
      
            FFval(max1)=-1;
            max2=maximum(FFval);
            Amax2=Arr(max2);
            double grad1 [2*n];
            double grad2 [2*n];
            double tempgrad1 [2];
            double tempgrad2 [2];
      
            // Computation of the gradients in Amax1 and Amax2
            for (int j=0;j<n;j++) {
                dF1(tempgrad1,Amax1,j,U,sigma,dy);
                dF1(tempgrad2,Amax2,j,U,sigma,dy);
                grad1[2*j]=tempgrad1[0];
                grad1[2*j+1]=tempgrad1[1];   
                grad2[2*j]=tempgrad2[0];
                grad2[2*j+1]=tempgrad2[1];   
                //norm+=tempgrad[0]*tempgrad[0]+tempgrad[1]*tempgrad[1];
                //cout<<setprecision(15)<<tempgrad1[0]<<' '<<tempgrad1[1]<<' '<<tempgrad2[0]<<' '<<tempgrad2[1]<<' '<<U[j]<<' '<<sigma[j]<<' ';   
                manifold.open("results.serial.dat", ios_base::app); 
                manifold<<tempgrad1[0]<<' '<<tempgrad1[1]<<' '<<tempgrad2[0]<<' '<<tempgrad2[1]<<' ';      
                manifold.close(); 
                manifold.flush();
            }  
            //norm=sqrt(norm);
      
            // Computation of the increment of potential parameters (maj)
            double a1=0, a2=0;
            for (int j=0;j<n;j++) {
                a1+=U[j]*U[j]*(grad1[2*j]-grad2[2*j])*grad1[2*j]+sigma[j]*sigma[j]*(grad1[2*j+1]-grad2[2*j+1])*grad1[2*j+1];
                a2+=U[j]*U[j]*(grad2[2*j]-grad1[2*j])*grad2[2*j]+sigma[j]*sigma[j]*(grad2[2*j+1]-grad1[2*j+1])*grad2[2*j+1];
            }

            for (int j=0;j<n;j++) {
	            maj[2*j]=U[j]*(a1*grad2[2*j]+a2*grad1[2*j]);
                maj[2*j+1]=sigma[j]*(a1*grad2[2*j+1]+a2*grad1[2*j+1]);
            }   
        }
    
        // Gradient descent in the "manifold" for more than 2 maxima
        else {
            double tempgrad [2];
            arma::mat gradman(2*n,countman);
            for (arma::uword j=0;j<countman;j++) {			// filling a matrix with all the gradients in each maximum
                for (arma::uword i=0;i<n;i++) {
                    dF1(tempgrad,Arrman(j),i,U,sigma,dy);
                    gradman(2*i,j)=U[i]*tempgrad[0];			// logarithmic descent (although it makes probably no difference with the standard descent at small step)
                    gradman(2*i+1,j)=sigma[i]*tempgrad[1];
                }
            }

            arma::mat diffgrad(2*n,countman-1);			// now we construct the matrix containing the difference of adjacent gradients
            for (arma::uword j=0;j<countman-1;j++)  diffgrad.col(j)=gradman.col(j)-gradman.col(j+1);
            arma::vec coeffs0 = arma::solve(diffgrad.t() * diffgrad , diffgrad.t() * gradman.col(0));  // solve the linear system with the matrix of ovelap and the right vector //,solve_opts::no_approx
        arma::vec temporary0=diffgrad*coeffs0;
        for (arma::uword i=0;i<2*n;i++) maj[i]=gradman(i,0)-temporary0(i);			// compute the direction update    
        }
    

        // Research of the best step for the gradient descent of {log U_i, log sigma_i}
        bool stop=false;
        best=-1;
        double step;
        arma::uword countertemp;
        int max1temp;
        double Unew [n];
        double sigmanew [n];
        double tempphi;
        double tempU [n];
        double tempsigma[n];
        arma::vec FFvaltemp(m);
        arma::vec Arrtemp(m);
    
        for (int es=-least_search;es<=max_search;es++) {   
            step=pow(10,es);
            for (int j=0;j<n;j++) {							// temporary updates of U, sigma
                tempU[j]=U[j]*(1-step*maj[2*j]);
                tempsigma[j]=sigma[j]*(1-step*maj[2*j+1]);
                if (tempsigma[j]<=0) stop=true;
            }
            if (stop) { 
                cout << " Breaking from for loop. Cycle is: " << cycle << " es is: " << es << endl;
                
                break;
            } // step is too high probably, so skip to the next value

            countertemp=cages(FFvaltemp,Arrtemp,tempU,tempsigma,dy,incrA);
            max1temp=maximum(FFvaltemp);
            tempphi=1./FFvaltemp(max1temp);	// look for highest value of phi in this range of steps

            if (tempphi>phi) {	// note that if it is the same phi we will have the highest step since es is increasing
                phi=tempphi; 
                FFval=FFvaltemp;
                Arr=Arrtemp;
                max1=max1temp;
                Amax1=Arr(max1);
                FFmax1=FFval(max1);
                counter=countertemp;
                best=step;
                for (int j=0;j<n;j++) {
                    Unew[j]=tempU[j];
                    sigmanew[j]=tempsigma[j];
                }  
            }

        }  

        manifold.open("results.serial.dat", ios_base::app);
        manifold<<best<<endl;	// cout << [gradients_i << U_i << sigma_i]_i << norm << phi << step
        manifold.close(); 
        manifold.flush();

        cout << " Updating U and sigma , Cycle: " << cycle << endl;
        for (int j=0;j<n;j++) {U[j]=Unew[j]; sigma[j]=sigmanew[j];}					// update of U, sigma
    
    }
    t_end =  (seconds() - t_start)/60.0;

    manifold.open("results.serial.dat", ios_base::app);

    manifold << "\n Initialization:  " << t_init;
    manifold << "  Entire duration:  " << t_end << endl;
    manifold.close(); 
    manifold.flush();

    //manifold.close();
  
    return 0;
  
}


