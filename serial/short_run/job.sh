#!/bin/bash
#PBS -N Mani-serial-shrt
#PBS -l nodes=1
#PBS -l walltime=23:00:00
#PBS -q serial

module purge
module load gcc/4.9.0
module load intel/2017


cd $PBS_O_WORKDIR

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/emenkah/thibaud/armadillo-8.100.1

rm -f results.serial.dat

./manifold.arma.serial.x 2 3 8 10 3 0 

