#!/bin/bash
#PBS -N Longrun-par
#PBS -l nodes=1:ppn=12
#PBS -l walltime=23:56:00
#PBS -q long

module purge
module load openmpi/1.10.2/intel/2017


cd $PBS_O_WORKDIR

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/emenkah/thibaud/armadillo-8.100.1

rm -f results.par.dat 

mpirun -np 12 manifold.arma.par.x 2 3 8 10 2 0 > debug.output.txt 

