##!/bin/bash
##PBS -N Mani-Par-Short-Test
##PBS -l nodes=1:ppn=8
##PBS -l walltime=23:00:00
##PBS -q long

module purge
module load openmpi/1.10.2/intel/2013


cd $PBS_O_WORKDIR

ulimit -a unlimited 

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/emenkah/thibaud/armadillo-8.100.1

rm -f results.par.dat Mani-Par* output.txt

touch output.txt

mpirun -np 8 manifold.arma.par.x 2 3 8 10 3 0 > output.txt &
tail -f output.txt  

