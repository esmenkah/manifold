## Generating dense packings of hard spheres by soft interaction design. </br>


#### Parallelization of an extension of the gradient descent algorithm on manifolds where the gradient is discontinuous.